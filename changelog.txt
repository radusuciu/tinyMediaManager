Version 3.1
=======================
+ added trailer support for TV shows
+ added experimental support for BluRay ISO reading
+ completely reworked scraper configuration: you now have a better control what scraper fetches/updates
  DUE TO THE MASSIVE REWORK YOU MAY NEED TO REVIEW YOUR SCRAPER SETTINGS
+ support for named seasons
+ support for rendering animated gifs
+ support for movie set artwork style of Kodi v19
+ added a movie set artwork cleanup function (to rename/cleanup) the movie set artwork in the right naming scheme) #715 #694 #511
+ updated libmediainfo to 19.09
+ changed to nativefiledialog to avoid usage of JavaFX (better compatibility across different systems/Java versions)
+ visually enhanced the CheckComboBox
+ write the movie set/collection id as tmdbCollectionId for emby
+ added UPPER, lower and Title case renderer to the renamer. See our wiki for more information #680
+ added original title to the text search in TV shows
+ added updater script for command line #124
+ try to detect the season folder name by analyzing the episode filenames #699
+ added the new note field to the bulk editors
+ enabled sorting by multiple columns in the movie table
+ enabled writing of subtitles without language code #621
+ added file size to the renamer patterns #691
+ (Movies) added a bulk editor function to write the languages of the audio streams into the spoken language field #714
+ added an option to delete artwork from within the editor (movies, TV shows, episodes) #367
+ (Movies) added a new export template: DarkTemplate (thx @maxburon)
+ added a aspect ratio filter for common aspect ratios #644
+ IMDB: added option to scrape keywords (tags) #666
+ added regexp support in badwords
+ added navigation in the TV show tree by typing
+ added seasonXX-landscape naming scheme
x NFO: always write at least one id entry with default="true"
x do not move episode files if the episode and season renamer patterns are empty #667
x use temp files to create backups #678
x more/better HDR format detection (you need to reload MediaInformation of your files!)
x fix for writing Quicktime (Apple) trailers as .quicktime
x added 1440p to the video formats (and some common 1440p resolutions to the detection) #686
x changed date added logic: now it is configurable which date should be used #600
x respect TV show bad words in the TV show update data source logic #692
x do not rewrite NFO file, if nothing has changed #698 
x IMDb: parse release date from releaseinfo page too #697
x fixed TV show tag filter
x downgrade TVDB search result score when not receiving any year to compare
x changed writing of the outline in the NFO. It is now configurable how the outline should be written #630 #683
x enhanced movie renamer pattern validation #709 #705
x moved export logic into thread to do not block the UI while exporting
x IMDB: use the first found release date
x TMDB: show all search results (not only the first 20)
x TMDB: add season cast/crew to the episodes too
x TMDB: clean search string (move The, ... to the front on search)
x fixed scraping with MovieMeter.nl scraper (API changes on their side)
x updated the download missing artwork task to do a "light" cleanup of artwork files according to the settings
x the rating in the details panel now updates correctly
x fixed Youtube downloader
x fixed Mpdb.tv scaper
x do not open the image preview dialog multiple times #738
x IMDB: fixed checking of IMDB id
x parse plot/tagline from NFO respecting line breaks #742


Version 3.0.5
=======================
+ added a scraper for MPDb.tv (thx @wjanes)
+ added an action to force deletion of all HTTP caches (on disk and in memory) #656
+ added changing of the movie edition to the movie bulk editor
+ added a custom note field for movies/TV shows/episodes
+ added unwanted files dialog for TV shows
x show popupmenu from text fields on macOS #637
x crash on startup if some images could not be loaded
x catch more exceptions on file download #642
x do not try to open dialogs on disconnected screens #640
x delete subtitle archive after extracting #641
x fix update data sources from Google Drive
x correct date format style in files
x when unable to load the database, start over with a new/clean one rather than not starting tmm
x fixed loading of the colon replacement in the renamer settings
x enhanced detection of valid NFO files #638
x write the plot content to the outline field in the NFO #630
x ImageCache must not crash tmm on exceptions #659
x enabled to explicitly filter for empty values #660
x refactored the MediaPortal NFO format into 2 new formats for enhanced compatibility: MP MyVideos and MP Moving Pictures
x do not crash tmm on startup on incomplete settings #663
x remove a custom genre in the bulk editor does not throw an exception any more
x do not overwrite user ratings on scrape
x better logging of errors in the OpenSubtitles scraper #633
x fixed movie in a movie set filter


Version 3.0.4
=======================
+ changed "finding unwanted files" logic to use regular expressions
+ show a warning what rename&cleanup does
+ added a new export template (Mobile Movie Search, Soda's modifs) thx @Poka
x fix missing replacement of path separators in movie/Tv show renamer tokens #618
x fix parsing of episode vote count in the IMDB scraper
x fetching artwork sometimes returned the wrong type #617
x fixed wrong detection of user defined genres like Marvel
x fixed detection of IMAX edition
x renamed Trakt.tv scraper option "API key" to "Client ID"
x detecting season poster from folder.jpg #626
x fixed removing of an underscore at the beginning of the renamer token #619
x added trailer file names to presets


Version 3.0.3
=======================
+ reworked the YouTube download engine. Should now work for almost all trailers! (thx @wjanes)
+ separated space substitution for filenames and foldernames (movies) #611
+ added jump to movie by keystroke in the movie table
+ added Mexican Spanish to scraper languages #570
+ added Icelandic to scraper languages #574
+ added a duplicate episode filter #578
+ added a MovieEdition filter
+ added option to store tmm data into another folders http://bit.ly/2JoUUeN
+ added seconds to the runtime in the media files panel
+ added an option to hide media logos (to save space for low resolution devices)
+ reworked logic for finding the best artwork according to the settings #589
+ added first aired to the TV show tree #557
+ added a button to revert renamer template to the default value
+ added option to explicitly rebuild the image cache for selected movies/TV shows
+ added "Final Cut" to recognized movie edition patterns #612
x fixed downloading movie set artwork in the right resolution #580
x write user ratings to own tags in the NFO files
x write correct trailer url to the NFO
x do not write stream runtime for disc style movies to NFO
x sort tags in the tag filter dropdown
x enlarged the year column that occasional truncation should not happen again
x the logos panel does not push other fields (like play button, plot, ..) out of the screen
x removed duplicate shortcut CTRL+SHIFT+U #590
x do not throw exception if a TV show has x*100 episodes #587
x do not resize extrathumbs if the setting is deactivated #589
x replace colons in the renamer patterns
x enhanced the renamer replacements/cleanup #597
x execute a copy&delete if a move fails #602
x getMainVideoFile() now returns the first part of stacked movies/episodes rather than the biggest one
x fixed regexp to find extra(s) in nested movie folders #603
x fixed renaming of episodes with samples #601
x fixed crash in the movie renamer settings when a movie was in a UNC root #561
x fixed import of MediaInfo.xml files
x fixed TV show renamer spaces substitution
x fixed ${parent} processing in the renamer #609
x fixed removing of audio streams/subtitles in the media file editor #610


Version 3.0.2
=======================
+ added undo/redo for all text fields
+ added renamer token for the current parent folder (to enable renaming of folders without moving them in the folder structure)
x read/write TV show sorttitle from/to NFO #539
x better detection of same named movies in different quality
x fix bulk downloading images (AniDB, Kyra Animated)
x better 3D detection from filename
x fixed some scaling issues in the renamer settings panels
x set episode year along with firstAired
x writing of TV show actor images
x fix an occasional crash when scraping movie sets #563
x fixed language detection for Turkish subtitles #562
x reworked KodiRPC connection (better datasource matching) #554 #549
x fixed location for writing NFO and trailer files (for disc folders)
x changed hotkey for deleting movies/TV shows to CTRL+SHIFT+DEL #568
x fixed parsing of votes from IMDB in another notation #559
x clean episode title upon importing


Version 3.0.1
=======================
x fixed Fanart.tv reading personal clientKey
x fixed Fanart.tv using TMDB id fallback
x fixed TvShow exporter naming pattern #525
x fixed TvShow renamer (Synology TvShow metadata overwriting videos) #527
x fixed scraping of episode cast with IMDB
x fixed displaying of episode director/writers in the episode editor
x fixed downloading of season artwork #522
x removed VIDEO_TS/BDMV NFO styles (integrated these into the other two styles)
x updated JNA and libmediainfo
x do not print "Unknown" for empty media sources
x fixed artwork fetching with tvdb and removed bad thumb urls from the DB (you may need to re-write your NFO files)
x fixed occasional Java crashes on OSX
x fixed occasional error popup on start of tinyMediaManager
x changed temporary artwork download to the systems temp folder


Version 3.0
=======================

+ completely rewritten UI (thx to @joostzilla)
  - new style and layout (better usage of the available space - especially for low screen devices)
  - flexible and configurable tables
  - improved filters (inclusive and exclusive filters)
  - better UI scaling on high dpi screens
+ added a dark theme
+ removed donator version (but donations are still highly appreciated)
+ completely rewritten the renamer engine
+ completely rewritten NFO parsing and writing (much more flexible now)
+ easier translation via weblate.org now (https://hosted.weblate.org/projects/tinymediamanager/)
+ increased required Java version to Java 8+
+ ability to mix in/search for missing episodes
+ presets in settings for common media centers (Kodi, old XBMC, Plex, MediaPortal)
+ added a feature to find "unwanted" files for cleanup (e.g. .txt, .url, ...)
+ many enhancements under the hood
